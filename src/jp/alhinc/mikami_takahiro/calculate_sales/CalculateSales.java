package jp.alhinc.mikami_takahiro.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class CalculateSales {
	public static void main(String[] args) {
		Map<String, String> branchMap = new HashMap<String, String>();
		Map<String, Long> salesMap = new HashMap<String, Long>();
		BufferedReader br = null;
		try {
			File file = new File(args[0], "branch.lst");
			if (!file.exists()) {
				System.out.println("支店定義ファイルが存在しません");
				return;
			}
			br = new BufferedReader(new FileReader(file));
			String line;
			while ((line = br.readLine()) != null) {
				String[] store = line.split(",");
				if (!store[0].matches("[0-9]{3}$")|| store.length != 2)  {
					System.out.println("支店定義ファイルのフォーマットが不正です");
					return;
				}
				String branchCode = store[0];
				String branchName = store[1];
				branchMap.put(branchCode, branchName);
			}
			File dir = new File(args[0]);
			File[] filenames = dir.listFiles();
			Arrays.sort(filenames);
			int preNumber = -1;
			for (int i = 0; i < filenames.length; i++) {
				if (!filenames[i].getName().matches("^[0-9]{8}\\.rcd$") || !filenames[i].isFile()) {
					continue;
				}

				String fileNumber = filenames[i].getName().substring(0, 8);
				int currentNumber = Integer.parseInt(fileNumber);
				if(preNumber != -1 && currentNumber - preNumber != 1) {
					System.out.println("売上ファイル名が連番になっていません");
					return;
				}

				br = new BufferedReader(new FileReader(filenames[i]));
				String branch = br.readLine();
				String sales = br.readLine();
				if(!branchMap.containsKey(branch)){
					System.out.println(filenames[i].getName() + "の支店コードが不正です");
					return;
				}
				if(br.readLine() != null) {
					System.out.println( filenames[i].getName() + "のフォーマットが不正です");
					return;
				}
				long branchSales = Long.parseLong(sales);
				if (salesMap.containsKey(branch)) {
					long sum = branchSales + salesMap.get(branch);
					salesMap.put(branch, sum);
				}else {
					salesMap.put(branch, branchSales);
					salesMap.get(branch);
				}
				String disitResult = String.valueOf(branchSales);
				if(disitResult.length() > 10) {
					System.out.println("合計金額が10桁を超えました");
					return;
				}
				String secondNumber = filenames[i].getName().substring(0, 8);
				int nextNumber = Integer.parseInt(secondNumber);
				preNumber = nextNumber;
			}
			File branchout = new File(args[0], "branch.out");
			PrintWriter printoutFile = new PrintWriter(new BufferedWriter(new FileWriter(branchout)));
			for (String storeCode : branchMap.keySet()) {
				printoutFile.println(storeCode + "," + branchMap.get(storeCode) + "," + salesMap.get(storeCode));
			}
			printoutFile.close();
		}catch (IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return;
		} finally {
			if (br != null) {
			try {
				br.close();
			} catch (IOException e) {
				System.out.println("予期せぬエラーが発生しました");
				return;
			}

			}
		}

	}
}
